from django.shortcuts import render, redirect
from .models import Curso

# Create your views here.
def home(request):
    cursos = Curso.objects.all()
    return render(request, 'gestionCurso.html', {"cursos": cursos})

def registrarCurso(request):
    codigo = request.POST['txtCodigo']
    nombre = request.POST['txtNombre']
    credito = request.POST['numCredito']
    imagen = request.POST['img']
    
    curso= Curso.objects.create(
        codigo= codigo, nombre= nombre, credito= credito)
    return redirect('/')

def eliminarCurso(request, codigo):
    curso = Curso.objects.get(codigo=codigo)
    curso.delete()
    return redirect('/')

def editarCurso(request, codigo):
    curso = Curso.objects.get(codigo=codigo)
    return render(request, "editarCurso.html", {"curso" : curso})

def edicionCurso(request):
    codigo = request.POST['txtCodigo']
    nombre = request.POST['txtNombre']
    credito = request.POST['numCredito']
    curso = Curso.objects.get(codigo=codigo)
    curso.nombre = nombre
    curso.credito = credito
    curso.save()
    return redirect('/')
    