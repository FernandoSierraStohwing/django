from django.db import models

# Create your models here.
class Curso(models.Model):
    codigo = models.CharField(primary_key=True, max_length=50)
    nombre = models.CharField(max_length=100)
    credito = models.SmallIntegerField()
    imagen = models.FileField()
    
    
    def __str__(self):
        texto = "{0} ({1})"
        return texto.format(self.nombre, self.credito)
